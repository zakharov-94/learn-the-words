import React from 'react';
import ReactDom from 'react-dom';
import HeaderBlock from './components/HeaderBlock/index'
import './index.css';

const AppList = () => {
  const items = ['Item1', 'Item2', 'Item3', 'Item4'];
  const firstItem = <li>Item 0</li>;

  const isAuth = true;

  return (
    <ul>
      { isAuth ? firstItem : null }
      { items.map(item => <li>{ item }</li>) }
    </ul>
  );
}

const AppHeader = () => {
  return (
    <h1 className="header">My header</h1>
  );
}

const AppInput = () => {
  const placeholder = 'Type text...';
  
  return (
    <label htmlFor='search'>
      <input placeholder={placeholder}
            id='search' />
    </label>
  );
}

const App = () => {
  return (
    <>
      <HeaderBlock />
      <AppHeader/>
      <AppInput/>
      <AppList />
      <AppHeader/>
      <AppList />
    </>
  );
}

ReactDom.render(<App />, document.getElementById('root'));
